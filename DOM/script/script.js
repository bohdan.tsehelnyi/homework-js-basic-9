// =============THEORY=========================
/*
1. Опишіть своїми словами що таке Document Object Model (DOM).
DOM - це інтерфейс, модель якого побудована з деревовидної структури обєктів, які можна отримувати з допомогою методів та вводити динамічні способи їх маніпуляцій. 

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - виводить теги разом з контентом, а також вводить нові теги та відтворює їх відповідно до їх функціоналу. 
innerText працює по принципу textContent. Він отримує лише контент без тегів та при введені нових тегів відображає їх як текст.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Є два напрямки звернення до елемента сторінки:
getElement... (отримуємо живі елементи)
querySelector... (статичний напрямок)

Найпопулярніший:
querySelectorAll оскільки з його допомогою ти отримуєш всі елементи з тотожною назвою (атрибуту, ідентифікатора, классу, тегу).
Але ми використовуємо напрямки відповідно до завдання.

4. Яка різниця між nodeList та HTMLCollection?

Це схожі на масив колекції вузлів. 
nodeList (querySelectotAll) може зберігати всі типи вузлів (тексти, коментарі).
HTMLColection (getElement...) може зберігати лише вузли HTML елементів.
*/

// =================PRACTICE==========================

// 1.

const feature = document.getElementsByClassName('feature');
const featureTwo = document.querySelectorAll('.feature');
console.log(feature);
console.log(featureTwo);

for (let el of feature){
	el.style.textAlign = "center";
}

// 2.

const title = document.getElementsByTagName('h2');

for (let el of title){
	el.textContent = "Awesome feature";
}

// 3. 

const subtitle = document.querySelectorAll('.feature-title');
console.log(subtitle);

subtitle.forEach((el) => {
	 el.innerHTML += "!";
});